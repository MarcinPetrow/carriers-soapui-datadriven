package com.metapack.soapui.teststeps.datadrivenloop;

import com.eviware.soapui.model.ModelItem;
import com.eviware.soapui.model.testsuite.TestProperty;
import org.apache.xmlbeans.SchemaType;

import javax.xml.namespace.QName;

public class DataDrivenLoopTestStepProperty implements TestProperty {

    private String name;
    private String value;
    private String description;
    private String defaultValue;

    public DataDrivenLoopTestStepProperty(String name, String value) {
        this.name = name;
        description = "";
        defaultValue = "";
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setValue(String newValue) {
        value = newValue;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public QName getType() {
        return null;
    }

    @Override
    public ModelItem getModelItem() {
        return null;
    }

    @Override
    public boolean isRequestPart() {
        return false;
    }

    @Override
    public SchemaType getSchemaType() {
        return null;
    }
}

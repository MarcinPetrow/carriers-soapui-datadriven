package com.metapack.soapui.teststeps.datadrivenloop.models;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class DataDrivenLoopCellRenderer extends DefaultTableCellRenderer {

    public DataDrivenLoopCellRenderer() {
        setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        String stringValue = (String) table.getValueAt(row, table.getColumnModel().getColumnCount() - 1);

        if (isSelected) {
            setForeground(new Color(68, 69, 71));
            setBackground(new Color(186, 190, 196));
        } else if (stringValue.equals("FAILED")) {
            setForeground(new Color(99, 99, 99));
            setBackground(new Color(255, 211, 211));
        } else if (stringValue.equals("OK")) {
            setForeground(new Color(99, 99, 99));
            setBackground(new Color(243, 255, 237));
        } else {
            setForeground(new Color(58, 58, 58));
            setBackground(new Color(252, 252, 252));
        }

        setText(value != null ? value.toString() : "");
        return this;
    }
}
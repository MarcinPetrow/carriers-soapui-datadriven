package com.metapack.soapui.teststeps.datadrivenloop.models;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

public class DataDrivenLoopResultsTableModel extends DefaultTableModel {

    public DataDrivenLoopResultsTableModel(ArrayList<String> headers) {
        columnIdentifiers.clear();
        columnIdentifiers.addAll(headers);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}

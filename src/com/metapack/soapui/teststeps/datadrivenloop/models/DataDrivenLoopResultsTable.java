package com.metapack.soapui.teststeps.datadrivenloop.models;

import java.util.ArrayList;

public class DataDrivenLoopResultsTable extends ArrayList<ArrayList<String>> {

    private ArrayList<String> headers;

    public DataDrivenLoopResultsTable(ArrayList<String> headers) {
        this.headers = headers;
    }

    public ArrayList<String> getHeaders() {
        return headers;
    }
}

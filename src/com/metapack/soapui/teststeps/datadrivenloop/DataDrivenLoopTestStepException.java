package com.metapack.soapui.teststeps.datadrivenloop;

public class DataDrivenLoopTestStepException extends Exception {

    public DataDrivenLoopTestStepException(String message) {
        super(message);
    }
}

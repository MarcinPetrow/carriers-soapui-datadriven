package com.metapack.soapui.teststeps.datadrivenloop;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepWithProperties;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansion;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansionContainer;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansionUtils;
import com.eviware.soapui.model.testsuite.TestCaseRunContext;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.model.testsuite.TestStepResult.TestStepStatus;
import com.eviware.soapui.support.UISupport;
import com.eviware.soapui.support.xml.XmlObjectConfigurationBuilder;
import com.eviware.soapui.support.xml.XmlObjectConfigurationReader;
import com.metapack.soapui.teststeps.datadrivenloop.helpers.DataDrivenLoopTestStepHelper;
import com.metapack.soapui.teststeps.datadrivenloop.models.DataDrivenLoopResultRow;
import com.metapack.soapui.teststeps.datadrivenloop.models.DataDrivenLoopResultsTable;
import com.metapack.soapui.teststeps.datadrivensource.DataDrivenSourceTestStep;

import java.util.ArrayList;
import java.util.List;

public class DataDrivenLoopTestStep extends WsdlTestStepWithProperties implements PropertyExpansionContainer {

    private String dataDrivenSourceTestStep = "";
    private DataDrivenLoopResultsTable results;
    private DataDrivenLoopTestStepDesktopPanel resultsDesktopPanel;

    public DataDrivenLoopTestStep(WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
        super(testCase, config, true, forLoadTest);

        if (!forLoadTest) {
            setIcon(UISupport.createImageIcon("DataDrivenLoopTestStep/icons/default.png"));
        }

        if (config.getConfig() != null) {
            readConfig(config);
        }
    }

    @Override
    public TestStepResult run(TestCaseRunner runner, TestCaseRunContext context) {
        WsdlTestStepResult result = new WsdlTestStepResult(this);
        result.startTimer();
        try {
            DataDrivenSourceTestStep sourceTestStep = (DataDrivenSourceTestStep) getTestCase().getTestStepByName(dataDrivenSourceTestStep);
            if (dataDrivenSourceTestStep.isEmpty()) {
                throw new DataDrivenLoopTestStepException("Empty data source test step. Please check configuration.");
            } else if (sourceTestStep == null) {
                throw new DataDrivenLoopTestStepException("Could not find data source with name: \"" + dataDrivenSourceTestStep + "\". Please check configuration.");
            }

            String currentRowNumberPropertyName = dataDrivenSourceTestStep + "currentRowNumber";
            String numberOfRowsPropertyName = dataDrivenSourceTestStep + "numberOfRows";

            Object currentRowNumber = context.getProperty(currentRowNumberPropertyName);
            Object numberOfRows = context.getProperty(numberOfRowsPropertyName);

            if (currentRowNumber != null || numberOfRows != null) {
                int currentRowNumberInt = Integer.valueOf(String.valueOf(currentRowNumber));
                int numberOfRowsInt = Integer.valueOf(String.valueOf(numberOfRows));
                if (currentRowNumberInt == 2) {
                    results = new DataDrivenLoopResultsTable(DataDrivenLoopTestStepHelper.getHeadersForGUI(this));
                    clearDataOnDesktopPanel();
                }
                addResult(context);
                if (currentRowNumberInt <= numberOfRowsInt && !stopProcessing(context)) {
                    context.getTestRunner().gotoStepByName(dataDrivenSourceTestStep);
                } else {
                    context.removeProperty(currentRowNumberPropertyName);
                    context.removeProperty(numberOfRowsPropertyName);
                    restoreTestCaseSettings(context);
                }
            }

            setIcon(UISupport.createImageIcon("DataDrivenLoopTestStep/icons/passed.png"));
            result.setStatus(TestStepStatus.OK);
        } catch (Throwable ex) {
            restoreTestCaseSettings(context);
            setIcon(UISupport.createImageIcon("DataDrivenLoopTestStep/icons/failed.png"));
            SoapUI.logError(ex);
            result.setError(ex);
            result.setStatus(TestStepStatus.FAILED);
        }

        result.stopTimer();
        return result;
    }

    private void clearDataOnDesktopPanel(){
        if(resultsDesktopPanel != null){
            resultsDesktopPanel.clearData();
        }
    }

    private void addResult(TestCaseRunContext context) {
        ArrayList<String> testSteps = DataDrivenLoopTestStepHelper.getTestStepList(this);
        for (int i = 0; i < testSteps.size(); i++) {
            DataDrivenLoopResultRow row = new DataDrivenLoopResultRow();
            row.add(testSteps.get(i));
            for (String property : DataDrivenLoopTestStepHelper.getHeadersForResults(this)) {
                String propertyValue = getTestCase().getTestStepByName(dataDrivenSourceTestStep).getPropertyValue(property);
                row.add(propertyValue);
            }
            row.add(DataDrivenLoopTestStepHelper.getResultOfTestStep(context, testSteps.get(i)));
            results.add(row);
            if(resultsDesktopPanel != null){
                resultsDesktopPanel.addRow(row);
            }
        }
    }

    private Boolean stopProcessing(TestCaseRunContext context) {
        Object failOnErrorObject = context.getProperty("FailOnError");
        Boolean FailOnError = (Boolean) failOnErrorObject;
        if (FailOnError) {
            ArrayList<String> testSteps = DataDrivenLoopTestStepHelper.getTestStepList(this);
            for (int i = 0; i < testSteps.size(); i++) {
                String result = DataDrivenLoopTestStepHelper.getResultOfTestStep(context, testSteps.get(i));
                if (result.equals(TestStepStatus.FAILED.toString())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void restoreTestCaseSettings(TestCaseRunContext context) {
        // Restore "Abort on Error" and "Fail TestCase on error:" test case settings
        Object failOnErrorObject = context.getProperty("FailOnError");
        Boolean FailOnError = (Boolean) failOnErrorObject;
        getTestCase().setFailOnError(FailOnError);
        Object failTestCaseOnErrorsObject = context.getProperty("FailTestCaseOnErrors");
        Boolean failTestCaseOnErrors = (Boolean) failTestCaseOnErrorsObject;
        getTestCase().setFailTestCaseOnErrors(failTestCaseOnErrors);
    }

    private void readConfig(TestStepConfig config) {
        XmlObjectConfigurationReader reader = new XmlObjectConfigurationReader(config.getConfig());
        dataDrivenSourceTestStep = reader.readString("dataDrivenSourceTestStep", "");
    }

    private void updateConfig() {
        XmlObjectConfigurationBuilder builder = new XmlObjectConfigurationBuilder();
        builder.add("dataDrivenSourceTestStep", dataDrivenSourceTestStep);
        getConfig().setConfig(builder.finish());
    }

    @Override
    public PropertyExpansion[] getPropertyExpansions() {
        List<PropertyExpansion> result = new ArrayList<>();
        result.addAll(PropertyExpansionUtils.extractPropertyExpansions(this, this, "dataDrivenSourceTestStep"));
        return result.toArray(new PropertyExpansion[result.size()]);
    }

    public String getDataDrivenSourceTestStep() {
        return dataDrivenSourceTestStep;
    }

    public void setDataDrivenSourceTestStep(String testStep) {
        dataDrivenSourceTestStep = testStep;
        addProperty(new DataDrivenLoopTestStepProperty("dataSourceTestStep", testStep));
        updateConfig();
    }

    public void setResults(DataDrivenLoopResultsTable results) {
        this.results = results;
    }

    public DataDrivenLoopResultsTable getResults() {
        return results;
    }

    public void setResultsDesktopPanel(DataDrivenLoopTestStepDesktopPanel resultsDesktopPanel) {
        this.resultsDesktopPanel = resultsDesktopPanel;
    }

    // Only for build artifact from IntelliJ IDEA
    public static void main(String[] args) {
    }
}

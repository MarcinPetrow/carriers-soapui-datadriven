package com.metapack.soapui.teststeps.datadrivenloop;

import com.eviware.soapui.impl.EmptyPanelBuilder;
import com.eviware.soapui.model.PanelBuilder;
import com.eviware.soapui.model.util.PanelBuilderFactory;
import com.eviware.soapui.ui.desktop.DesktopPanel;

public class DataDrivenLoopTestStepPanelBuilderFactory implements PanelBuilderFactory<DataDrivenLoopTestStep> {

    @Override
    public PanelBuilder<DataDrivenLoopTestStep> createPanelBuilder() {
        return new DataDrivenLoopTestStepPanelBuilder();
    }

    @Override
    public Class<DataDrivenLoopTestStep> getTargetModelItem() {
        return DataDrivenLoopTestStep.class;
    }

    public static class DataDrivenLoopTestStepPanelBuilder extends EmptyPanelBuilder<DataDrivenLoopTestStep> {
        @Override
        public DesktopPanel buildDesktopPanel(DataDrivenLoopTestStep modelItem) {
            return new DataDrivenLoopTestStepDesktopPanel(modelItem);
        }

        @Override
        public boolean hasDesktopPanel() {
            return true;
        }
    }
}

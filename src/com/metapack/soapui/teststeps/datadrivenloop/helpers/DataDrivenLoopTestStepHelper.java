package com.metapack.soapui.teststeps.datadrivenloop.helpers;

import com.eviware.soapui.model.testsuite.TestCaseRunContext;
import com.eviware.soapui.model.testsuite.TestProperty;
import com.metapack.soapui.teststeps.datadrivenloop.DataDrivenLoopTestStep;

import java.util.ArrayList;
import java.util.Map;

public class DataDrivenLoopTestStepHelper {
    public static ArrayList<String> getHeadersForGUI(DataDrivenLoopTestStep dataDrivenLoopTestStep) {
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Test step");
        Map<String, TestProperty> properties = dataDrivenLoopTestStep.getTestCase().getTestStepByName(dataDrivenLoopTestStep.getDataDrivenSourceTestStep()).getProperties();
        for (TestProperty testProperty : properties.values()) {
            headers.add(testProperty.getName());
        }
        headers.add("Result");
        return headers;
    }

    public static ArrayList<String> getHeadersForResults(DataDrivenLoopTestStep dataDrivenLoopTestStep) {
        ArrayList<String> headers = new ArrayList<>();
        Map<String, TestProperty> properties = dataDrivenLoopTestStep.getTestCase().getTestStepByName(dataDrivenLoopTestStep.getDataDrivenSourceTestStep()).getProperties();
        for (TestProperty testProperty : properties.values()) {
            headers.add(testProperty.getName());
        }
        return headers;
    }

    public static ArrayList<String> getTestStepList(DataDrivenLoopTestStep dataDrivenLoopTestStep) {
        ArrayList<String> testStepLists = new ArrayList<>();
        int startIndex = dataDrivenLoopTestStep.getTestCase().getTestStepIndexByName(dataDrivenLoopTestStep.getDataDrivenSourceTestStep()) + 1;
        int endIndex = dataDrivenLoopTestStep.getTestCase().getTestStepIndexByName(dataDrivenLoopTestStep.getName()) - 1;
        for (int i = startIndex; i <= endIndex; i++) {
            if (!dataDrivenLoopTestStep.getTestCase().getTestStepAt(i).isDisabled()) {
                testStepLists.add(dataDrivenLoopTestStep.getTestCase().getTestStepAt(i).getName());
            }
        }
        return testStepLists;
    }

    public static String getResultOfTestStep(TestCaseRunContext context, String testStepName) {
        for (int i = context.getTestRunner().getResults().size() - 1; i >= 0; i--) {
            if (context.getTestRunner().getResults().get(i).getTestStep().getName().equals(testStepName)) {
                return context.getTestRunner().getResults().get(i).getStatus().toString();
            }
        }
        return null;
    }
}

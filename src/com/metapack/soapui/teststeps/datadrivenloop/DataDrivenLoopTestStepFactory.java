package com.metapack.soapui.teststeps.datadrivenloop;

import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.impl.wsdl.teststeps.registry.WsdlTestStepFactory;

public class DataDrivenLoopTestStepFactory extends WsdlTestStepFactory {

    private static final String DATADRIVENLOOP_STEP_ID = "DATADRIVENLOOP";

    public DataDrivenLoopTestStepFactory() {
        super(DATADRIVENLOOP_STEP_ID, "Data driven loop", "Data driven loop test step", "DataDrivenLoopTestStep/icons/default.png");
    }

    @Override
    public WsdlTestStep buildTestStep(WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
        return new DataDrivenLoopTestStep(testCase, config, forLoadTest);
    }

    @Override
    public boolean canCreate() {
        return true;
    }

    @Override
    public TestStepConfig createNewTestStep(WsdlTestCase testCase, String name) {
        TestStepConfig testStepConfig = TestStepConfig.Factory.newInstance();
        testStepConfig.setType(DATADRIVENLOOP_STEP_ID);
        testStepConfig.setName(name);
        return testStepConfig;
    }
}
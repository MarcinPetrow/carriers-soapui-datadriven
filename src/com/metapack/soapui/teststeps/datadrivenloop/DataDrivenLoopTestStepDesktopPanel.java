package com.metapack.soapui.teststeps.datadrivenloop;

import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.support.UISupport;
import com.eviware.soapui.ui.support.ModelItemDesktopPanel;
import com.metapack.soapui.teststeps.datadrivenloop.models.DataDrivenLoopCellRenderer;
import com.metapack.soapui.teststeps.datadrivenloop.models.DataDrivenLoopResultsTableModel;
import com.metapack.soapui.teststeps.datadrivensource.DataDrivenSourceTestStep;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Vector;

public class DataDrivenLoopTestStepDesktopPanel extends ModelItemDesktopPanel<DataDrivenLoopTestStep> {

    private JComboBox<String> dataDrivenSourceComboBox;
    private DefaultComboBoxModel<String> dataDrivenSourceModel;
    private JLabel dataDrivenSourceLabel;
    private JPanel headerBackgroundPanel;
    private JPanel headerLinePanel;
    private JLabel headerLabel;
    private JLabel headerLogoLabel;
    private JScrollPane resultsTableScrollPane1;
    private JLabel resultsLabel;
    private JTable resultsTable;
    private JLabel filterLabel;
    private JCheckBox okCheckBox;
    private JCheckBox failedCheckBox;

    public DataDrivenLoopTestStepDesktopPanel(DataDrivenLoopTestStep modelItem) {
        super(modelItem);
        getModelItem().setResultsDesktopPanel(this);
        initComponents();
        initialize();
    }

    private void initialize() {
        formatResultsTable();
        fillResultsTableData();
    }

    public void clearData() {
        ArrayList<String> headers = getModelItem().getResults().getHeaders();
        resultsTable.setModel(new DataDrivenLoopResultsTableModel(headers));
    }

    public void addRow(ArrayList<String> values) {
        DataDrivenLoopResultsTableModel model = (DataDrivenLoopResultsTableModel) resultsTable.getModel();
        Vector row = new Vector();
        row.addAll(values);
        if (row.get(row.size() - 1).toString().equals(TestStepResult.TestStepStatus.FAILED.toString()) && failedCheckBox.isSelected()) {
            model.addRow(row);
        } else if (row.get(row.size() - 1).toString().equals(TestStepResult.TestStepStatus.OK.toString()) && okCheckBox.isSelected()) {
            model.addRow(row);
        }
    }

    public void fillResultsTableData() {
        if (getModelItem().getResults() != null) {
            {
                clearData();
                DataDrivenLoopResultsTableModel model = (DataDrivenLoopResultsTableModel) resultsTable.getModel();
                for (int i = 0; i < getModelItem().getResults().size(); i++) {
                    Vector values = new Vector();
                    values.addAll(getModelItem().getResults().get(i));
                    if (values.get(values.size() - 1).toString().equals(TestStepResult.TestStepStatus.FAILED.toString()) && failedCheckBox.isSelected()) {
                        model.addRow(values);
                    } else if (values.get(values.size() - 1).toString().equals(TestStepResult.TestStepStatus.OK.toString()) && okCheckBox.isSelected()) {
                        model.addRow(values);
                    }
                }
            }
        }
    }


    private void formatResultsTable() {
        resultsTable.setRowSelectionAllowed(true);
        resultsTable.setColumnSelectionAllowed(false);
        resultsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        resultsTable.setAutoCreateRowSorter(true);
        resultsTable.setDefaultRenderer(Object.class, new DataDrivenLoopCellRenderer());
        resultsTable.setShowGrid(false);
    }

    private void initComponents() {

        headerBackgroundPanel = new JPanel();
        headerLogoLabel = new JLabel();
        headerLabel = new JLabel();
        headerLinePanel = new JPanel();

        dataDrivenSourceLabel = new JLabel();
        dataDrivenSourceComboBox = new JComboBox<>();
        dataDrivenSourceModel = new DefaultComboBoxModel<>();
        fillDataDrivenSourceTestStepList();
        dataDrivenSourceComboBox.setModel(dataDrivenSourceModel);
        dataDrivenSourceComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                changeDataDrivenSourceTestStep(evt);
            }
        });

        resultsTableScrollPane1 = new JScrollPane();
        resultsLabel = new JLabel();
        resultsTable = new JTable();

        resultsTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{
                        {},
                        {},
                        {},
                        {}
                },
                new String[]{

                }
        ));
        resultsTableScrollPane1.setViewportView(resultsTable);

        buildFiltersSection();

        resultsLabel.setText("Results:");

        filterLabel.setText("Filter:");

        okCheckBox.setText("OK");

        failedCheckBox.setText("FAILED");

        setPreferredSize(new Dimension(650, 450));

        headerBackgroundPanel.setBackground(new Color(249, 249, 249));

        headerLogoLabel.setIcon(UISupport.createImageIcon("DataDrivenLoopTestStep/icons/header.png"));

        headerLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
        headerLabel.setText("Data driven loop");

        headerLinePanel.setBackground(java.awt.SystemColor.controlShadow);
        headerLinePanel.setPreferredSize(new java.awt.Dimension(1058, 1));

        GroupLayout headerBackgroudPanel1Layout = new GroupLayout(headerLinePanel);
        headerLinePanel.setLayout(headerBackgroudPanel1Layout);
        headerBackgroudPanel1Layout.setHorizontalGroup(
                headerBackgroudPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(0, 0, Short.MAX_VALUE)
        );
        headerBackgroudPanel1Layout.setVerticalGroup(
                headerBackgroudPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(0, 1, Short.MAX_VALUE)
        );

        GroupLayout headerBackgroudPanelLayout = new GroupLayout(headerBackgroundPanel);
        headerBackgroundPanel.setLayout(headerBackgroudPanelLayout);
        headerBackgroudPanelLayout.setHorizontalGroup(
                headerBackgroudPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(headerBackgroudPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(headerLogoLabel)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(headerLabel)
                                .addContainerGap(379, Short.MAX_VALUE))
                        .addComponent(headerLinePanel, GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
        );
        headerBackgroudPanelLayout.setVerticalGroup(
                headerBackgroudPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(headerBackgroudPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(headerBackgroudPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(headerLogoLabel)
                                        .addGroup(headerBackgroudPanelLayout.createSequentialGroup()
                                                .addGap(9, 9, 9)
                                                .addComponent(headerLabel)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(headerLinePanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );

        dataDrivenSourceLabel.setText("Data source:");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(headerBackgroundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(dataDrivenSourceLabel)
                                        .addComponent(resultsLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(dataDrivenSourceComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(resultsTableScrollPane1)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(filterLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(okCheckBox)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(failedCheckBox)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(headerBackgroundPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(dataDrivenSourceLabel)
                                        .addComponent(dataDrivenSourceComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(resultsLabel)
                                        .addComponent(filterLabel)
                                        .addComponent(okCheckBox)
                                        .addComponent(failedCheckBox))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(resultsTableScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                                .addContainerGap())
        );
    }

    private void buildFiltersSection() {
        filterLabel = new JLabel();
        okCheckBox = new JCheckBox();
        okCheckBox.setSelected(true);
        okCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                fillResultsTableData();
            }
        });
        failedCheckBox = new JCheckBox();
        failedCheckBox.setSelected(true);
        failedCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                fillResultsTableData();
            }
        });
    }

    private void fillDataDrivenSourceTestStepList() {
        dataDrivenSourceModel.removeAllElements();
        dataDrivenSourceModel.addElement("");
        boolean isOnList = false;
        for (DataDrivenSourceTestStep testStep : getModelItem().getTestCase().getTestStepsOfType(DataDrivenSourceTestStep.class)) {
            dataDrivenSourceModel.addElement(testStep.getName());
            if (testStep.getName().equals(getModelItem().getDataDrivenSourceTestStep())) {
                isOnList = true;
            }
        }
        if (isOnList) {
            dataDrivenSourceModel.setSelectedItem(getModelItem().getDataDrivenSourceTestStep());
        } else {
            getModelItem().setDataDrivenSourceTestStep("");
        }
    }

    private void changeDataDrivenSourceTestStep(ActionEvent evt) {
        String testStepName = String.valueOf(dataDrivenSourceComboBox.getSelectedItem());
        getModelItem().setDataDrivenSourceTestStep(testStepName);
    }
}

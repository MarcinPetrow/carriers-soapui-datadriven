package com.metapack.soapui.teststeps.datadrivensource;

public class DataDrivenSourceTestStepException extends Exception {

    public DataDrivenSourceTestStepException(String message) {
        super(message);
    }
}

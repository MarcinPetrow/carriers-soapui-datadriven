package com.metapack.soapui.teststeps.datadrivensource;

import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.impl.wsdl.teststeps.registry.WsdlTestStepFactory;

public class DataDrivenSourceTestStepFactory extends WsdlTestStepFactory {

    private static final String DATADRIVENSOURCE_STEP_ID = "DATADRIVENSOURCE";

    public DataDrivenSourceTestStepFactory() {
        super(DATADRIVENSOURCE_STEP_ID, "Data driven source", "Data driven source test step", "DataDrivenSourceTestStep/icons/default.png");
    }

    @Override
    public WsdlTestStep buildTestStep(WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
        return new DataDrivenSourceTestStep(testCase, config, forLoadTest);
    }

    @Override
    public boolean canCreate() {
        return true;
    }

    @Override
    public TestStepConfig createNewTestStep(WsdlTestCase testCase, String name) {
        TestStepConfig testStepConfig = TestStepConfig.Factory.newInstance();
        testStepConfig.setType(DATADRIVENSOURCE_STEP_ID);
        testStepConfig.setName(name);
        return testStepConfig;
    }
}
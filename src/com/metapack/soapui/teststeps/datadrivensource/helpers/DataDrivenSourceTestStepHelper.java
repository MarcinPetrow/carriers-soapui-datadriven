package com.metapack.soapui.teststeps.datadrivensource.helpers;

import com.eviware.soapui.SoapUI;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceRow;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceTable;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceTableModel;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

public class DataDrivenSourceTestStepHelper {
    public static final String CSVSeparatorForFile = ";";
    public static final String CSVSeparatorForSerialize = "<-|->";

    public static String serializeDataDrivenSourceTableToBase64String(DataDrivenSourceTable dataDrivenSourceTable) {
        StringBuilder content = new StringBuilder();
        for (int i = 0; i < dataDrivenSourceTable.size(); i++) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < dataDrivenSourceTable.get(i).size(); j++) {
                line.append(dataDrivenSourceTable.get(i).get(j));
                if (j != dataDrivenSourceTable.get(0).size() - 1) {
                    line.append(CSVSeparatorForSerialize);
                }
            }
            content.append(line);
            if (i != dataDrivenSourceTable.size() - 1) {
                content.append("\n");
            }
        }
        return serializeStringToBase64String(content.toString());
    }

    public static DataDrivenSourceTable deserializeBase64StringAsDataDrivenSourceTable(String value) {
        String deserialized = deserializeStringFromBase64String(value);
        String[] lines = deserialized.split("\r\n|\r|\n");
        DataDrivenSourceTable data = new DataDrivenSourceTable();
        for (String line : lines) {
            String[] fields = line.split(Pattern.quote(DataDrivenSourceTestStepHelper.CSVSeparatorForSerialize), -1);
            DataDrivenSourceRow row = new DataDrivenSourceRow();
            for (String field : fields) {
                row.add(field.trim());
            }
            data.add(row);
        }
        return data;
    }

    public static String saveDataDrivenSourceTableModelToCSVString(DataDrivenSourceTableModel dataDrivenSourceTableModel, Boolean skipEnableColumn) {
        String content = "";
        StringBuilder header = new StringBuilder();
        StringBuilder lines = new StringBuilder();
        int startIndex = skipEnableColumn ? 1 : 0;
        for (int i = startIndex; i < dataDrivenSourceTableModel.getColumnCount(); i++) {
            header.append(dataDrivenSourceTableModel.getColumnName(i));
            if (i != dataDrivenSourceTableModel.getColumnCount() - 1) {
                header.append(CSVSeparatorForFile);
            }
        }
        content += header;
        for (int i = 0; i < dataDrivenSourceTableModel.getRowCount(); i++) {
            StringBuilder line = new StringBuilder();
            for (int j = startIndex; j < dataDrivenSourceTableModel.getColumnCount(); j++) {
                line.append(dataDrivenSourceTableModel.getValueAt(i, j).toString());
                if (j != dataDrivenSourceTableModel.getColumnCount() - 1) {
                    line.append(CSVSeparatorForFile);
                }
            }
            lines.append("\n").append(line);
        }
        content += lines;
        return content;
    }

    public static String readFileToString(File file) throws Exception {
        FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fstream));
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line).append("\n");
        }
        bufferedReader.close();
        return content.toString();
    }

    public static String serializeStringToBase64String(String value) {
        byte[] encodedBytes = Base64.encodeBase64(value.getBytes(Charset.forName("UTF-8")));
        String encodedString = null;
        try {
            encodedString = new String(encodedBytes, "UTF-8");
        } catch (Throwable e) {
            SoapUI.logError(e);
        }
        return encodedString;
    }

    public static String deserializeStringFromBase64String(String value) {
        byte[] decodedBytes = Base64.decodeBase64(value.getBytes(Charset.forName("UTF-8")));
        String decodedString = "";
        try {
            decodedString = new String(decodedBytes, "UTF-8");
        } catch (Throwable ex) {
            SoapUI.logError(ex);
        }
        return decodedString;
    }

    public static String serializeDataSourceTableToBase64(DataDrivenSourceTable object) {
        String serialized = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(object);
            objectOutputStream.close();
            serialized = new String(Base64.encodeBase64(byteArrayOutputStream.toByteArray()));
        } catch (Exception ex) {
            SoapUI.logError(ex);
        }
        return serialized;
    }

    public static DataDrivenSourceTable deserializeDataSourceTableFromBase64(String serializedString) {
        DataDrivenSourceTable object = null;
        try {
            byte[] bytes = Base64.decodeBase64(serializedString.getBytes(Charset.forName("UTF-8")));
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
            object = (DataDrivenSourceTable) objectInputStream.readObject();
        } catch (Exception ex) {
            SoapUI.logError(ex);
        }
        return object;
    }
}

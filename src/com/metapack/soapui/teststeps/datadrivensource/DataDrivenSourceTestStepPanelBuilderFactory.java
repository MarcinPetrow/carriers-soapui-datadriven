package com.metapack.soapui.teststeps.datadrivensource;

import com.eviware.soapui.impl.EmptyPanelBuilder;
import com.eviware.soapui.model.PanelBuilder;
import com.eviware.soapui.model.util.PanelBuilderFactory;
import com.eviware.soapui.ui.desktop.DesktopPanel;

public class DataDrivenSourceTestStepPanelBuilderFactory implements PanelBuilderFactory<DataDrivenSourceTestStep> {

    @Override
    public PanelBuilder<DataDrivenSourceTestStep> createPanelBuilder() {
        return new DataDrivenSourceTestStepPanelBuilder();
    }

    @Override
    public Class<DataDrivenSourceTestStep> getTargetModelItem() {
        return DataDrivenSourceTestStep.class;
    }

    public static class DataDrivenSourceTestStepPanelBuilder extends EmptyPanelBuilder<DataDrivenSourceTestStep> {
        @Override
        public DesktopPanel buildDesktopPanel(DataDrivenSourceTestStep modelItem) {
            return new DataDrivenSourceTestStepDesktopPanel(modelItem);
        }

        @Override
        public boolean hasDesktopPanel() {
            return true;
        }
    }
}

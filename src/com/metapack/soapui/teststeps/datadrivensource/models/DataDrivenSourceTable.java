package com.metapack.soapui.teststeps.datadrivensource.models;

import java.io.Serializable;
import java.util.ArrayList;

public class DataDrivenSourceTable extends ArrayList<DataDrivenSourceRow> implements Serializable {

    public ArrayList<DataDrivenSourceRow> getEnabledRows() {
        ArrayList<DataDrivenSourceRow> rows = new ArrayList<>();
        for (int i = 1; i < size(); i++) {
            if (get(i).isEnabled()) {
                rows.add(get(i));
            }
        }
        return rows;
    }

    public DataDrivenSourceRow getHeader() {
        return get(0);
    }
}

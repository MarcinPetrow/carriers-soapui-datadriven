package com.metapack.soapui.teststeps.datadrivensource.models;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class DataDrivenSourceTableModel extends DefaultTableModel {

    public DataDrivenSourceTableModel() {
        columnIdentifiers.clear();
        columnIdentifiers.add("Enabled");
    }

    @Override
    public Class getColumnClass(int column) {
        if (column == 0) {
            return Boolean.class;
        }
        return String.class;
    }

    public void addColumn(String name) {
        Vector rows = dataVector;
        for (Object row : rows) {
            ((Vector) row).add("");
        }
        columnIdentifiers.add(name);
        fireTableStructureChanged();
    }

    public void removeColumn(String name) {
        int columnIndex = columnIdentifiers.indexOf(name);
        Vector rows = dataVector;
        for (Object row : rows) {
            ((Vector) row).remove(columnIndex);
        }
        columnIdentifiers.remove(columnIndex);
        fireTableStructureChanged();
    }

    public void editColumn(String name, String newName) {
        int columnIndex = columnIdentifiers.indexOf(name);
        columnIdentifiers.set(columnIndex, newName);
        fireTableStructureChanged();
    }

    public void moveColumn(String name, int position) {
        int actualColumnIndex = columnIdentifiers.indexOf(name);
        int newColumnIndex = columnIdentifiers.indexOf(name) + position;
        String columnNameA = (String) columnIdentifiers.get(actualColumnIndex);
        String columnNameB = (String) columnIdentifiers.get(newColumnIndex);
        columnIdentifiers.set(actualColumnIndex, columnNameB);
        columnIdentifiers.set(newColumnIndex, columnNameA);
        moveColumnData(actualColumnIndex, newColumnIndex);
        fireTableStructureChanged();
    }

    private void moveColumnData(int actualColumnIndex, int newColumnIndex) {
        for (int i = 0; i < getRowCount(); i++) {
            String a = (String) getValueAt(i, actualColumnIndex);
            String b = (String) getValueAt(i, newColumnIndex);
            setValueAt(a, i, newColumnIndex);
            setValueAt(b, i, actualColumnIndex);
        }
    }
}
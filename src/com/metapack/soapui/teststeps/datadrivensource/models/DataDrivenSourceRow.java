package com.metapack.soapui.teststeps.datadrivensource.models;

import java.io.Serializable;
import java.util.ArrayList;

public class DataDrivenSourceRow extends ArrayList<String> implements Serializable {
    public Boolean isEnabled() {
        return Boolean.valueOf(get(0));
    }
}

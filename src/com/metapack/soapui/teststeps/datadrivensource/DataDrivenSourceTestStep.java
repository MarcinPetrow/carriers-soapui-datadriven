package com.metapack.soapui.teststeps.datadrivensource;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepWithProperties;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansion;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansionContainer;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansionUtils;
import com.eviware.soapui.model.testsuite.*;
import com.eviware.soapui.support.UISupport;
import com.eviware.soapui.support.xml.XmlObjectConfigurationBuilder;
import com.eviware.soapui.support.xml.XmlObjectConfigurationReader;
import com.metapack.soapui.teststeps.datadrivensource.helpers.DataDrivenSourceTestStepHelper;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceRow;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceTable;

import java.util.ArrayList;
import java.util.List;

public class DataDrivenSourceTestStep extends WsdlTestStepWithProperties implements PropertyExpansionContainer {

    private DataDrivenSourceTable dataDrivenSourceTable;

    public DataDrivenSourceTestStep(final WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
        super(testCase, config, true, forLoadTest);
        if (!forLoadTest) {
            setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/default.png"));
        }

        if (config.getConfig() != null) {
            readConfig(config);
        }
    }

    @Override
    public TestStepResult run(TestCaseRunner runner, TestCaseRunContext context) {
        WsdlTestStepResult result = new WsdlTestStepResult(this);
        result.startTimer();
        try {
            String currentRowNumberPropertyName = getName() + "currentRowNumber";
            String numberOfRowsPropertyName = getName() + "numberOfRows";

            ArrayList<DataDrivenSourceRow> enabledRows = dataDrivenSourceTable.getEnabledRows();
            if (enabledRows == null || enabledRows.size() == 0) {
                throw new DataDrivenSourceTestStepException("Data source has no enabled data.");
            }

            Object currentRowNumber = context.getProperty(currentRowNumberPropertyName);
            Object numberOfRows = context.getProperty(numberOfRowsPropertyName);

            // First row processing
            if (currentRowNumber == null || numberOfRows == null) {
                context.setProperty(currentRowNumberPropertyName, 1);
                context.setProperty(numberOfRowsPropertyName, enabledRows.size());
                cleanProperties();
                for (int j = 1; j < dataDrivenSourceTable.getHeader().size(); j++) {
                    addProperty(new DataDrivenSourceTestStepProperty(dataDrivenSourceTable.getHeader().get(j), enabledRows.get(0).get(j)));
                }
                int nextRowNumberInt = 2;
                context.setProperty(currentRowNumberPropertyName, nextRowNumberInt);
                saveTestCaseSettings(context);

            } else { // Second row processing
                int currentRowNumberInt = Integer.valueOf(String.valueOf(currentRowNumber));
                for (int j = 1; j < dataDrivenSourceTable.getHeader().size(); j++) {
                    setPropertyValue(dataDrivenSourceTable.getHeader().get(j), enabledRows.get(currentRowNumberInt - 1).get(j));
                }
                int nextRowNumberInt = currentRowNumberInt + 1;
                context.setProperty(currentRowNumberPropertyName, nextRowNumberInt);
            }

            setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/passed.png"));
            result.setStatus(TestStepResult.TestStepStatus.OK);
        } catch (Throwable ex) {
            setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/failed.png"));
            SoapUI.logError(ex);
            result.setError(ex);
            result.setStatus(TestStepResult.TestStepStatus.FAILED);
        }

        result.stopTimer();
        return result;
    }

    private void saveTestCaseSettings(TestCaseRunContext context) {
        // Save current "Abort on Error" and "Fail TestCase on error:" test case settings
        context.setProperty("FailOnError", getTestCase().getFailOnError());
        getTestCase().setFailOnError(false);
        context.setProperty("FailTestCaseOnErrors", getTestCase().getFailTestCaseOnErrors());
        getTestCase().setFailTestCaseOnErrors(true);
    }

    private void readConfig(TestStepConfig config) {
        XmlObjectConfigurationReader reader = new XmlObjectConfigurationReader(config.getConfig());
        dataDrivenSourceTable = DataDrivenSourceTestStepHelper.deserializeBase64StringAsDataDrivenSourceTable(reader.readString("dataDrivenSourceTable", ""));
    }

    private void updateConfig() {
        XmlObjectConfigurationBuilder builder = new XmlObjectConfigurationBuilder();
        builder.add("dataDrivenSourceTable", DataDrivenSourceTestStepHelper.serializeDataDrivenSourceTableToBase64String(dataDrivenSourceTable));
        getConfig().setConfig(builder.finish());
    }

    @Override
    public PropertyExpansion[] getPropertyExpansions() {
        List<PropertyExpansion> result = new ArrayList<>();
        result.addAll(PropertyExpansionUtils.extractPropertyExpansions(this, this, "dataDrivenSourceTable"));
        return result.toArray(new PropertyExpansion[result.size()]);
    }

    public DataDrivenSourceTable getDataDrivenSourceTable() {
        return dataDrivenSourceTable;
    }

    public void setDataDrivenSourceTable(DataDrivenSourceTable dataDrivenSourceTable) {
        this.dataDrivenSourceTable = dataDrivenSourceTable;
        updateConfig();
    }

    private void cleanProperties() {
        ArrayList<String> propertiesNames = new ArrayList<>();
        for (TestProperty testProperty : getPropertyList()) {
            propertiesNames.add(testProperty.getName());
        }
        for (String property : propertiesNames) {
            deleteProperty(property, false);
        }
    }

    // Only for build artifact from IntelliJ IDEA
    public static void main(String[] args) {
    }
}

package com.metapack.soapui.teststeps.datadrivensource;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.support.UISupport;
import com.eviware.soapui.ui.support.ModelItemDesktopPanel;
import com.metapack.soapui.teststeps.datadrivensource.helpers.DataDrivenSourceTestStepHelper;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceRow;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceTable;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceFieldsListModel;
import com.metapack.soapui.teststeps.datadrivensource.models.DataDrivenSourceTableModel;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Vector;
import java.util.regex.Pattern;


public class DataDrivenSourceTestStepDesktopPanel extends ModelItemDesktopPanel<DataDrivenSourceTestStep> {

    private JButton AddNewLineButton;
    private JButton RemoveLineButton;
    private JButton addFieldButton;
    private JTable dataTable;
    private JButton deselectAllButton;
    private JButton editFieldButton;
    private JList<String> fieldsList;
    private JPanel headerBackgroudPanel;
    private JPanel headerBackgroudPanel1;
    private JLabel headerLabel;
    private JLabel headerLogoLabel;
    private JScrollPane fieldsListScrollPane1;
    private JScrollPane dataTableScrollPane;
    private JButton loadFromFileButton;
    private JButton moveDownFieldButton;
    private JButton moveLineDownButton;
    private JButton moveLineUpButton;
    private JButton moveUpFieldButton;
    private JButton removeFieldButton;
    private JButton saveToFileButton;
    private JButton selectAllButton;
    private DataDrivenSourceTableModel dataDrivenSourceTableModel;
    private DataDrivenSourceFieldsListModel fieldsListModel;

    public DataDrivenSourceTestStepDesktopPanel(DataDrivenSourceTestStep modelItem) {
        super(modelItem);
        initComponents();
        initialize();
        if (getModelItem().getDataDrivenSourceTable() != null) {
            deserializeDataFromDataDrivenSourceTable(getModelItem().getDataDrivenSourceTable());
        }
    }


    private void initialize() {
        dataTable.getTableHeader().setReorderingAllowed(false);
        ((DefaultTableCellRenderer) dataTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        fieldsListModel = new DataDrivenSourceFieldsListModel();
        fieldsList.setModel(fieldsListModel);
        fieldsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dataDrivenSourceTableModel = new DataDrivenSourceTableModel();
        dataDrivenSourceTableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                updateDataInConfig();
            }
        });
        dataTable.setModel(dataDrivenSourceTableModel);
        dataTable.setRowSelectionAllowed(true);
        dataTable.setColumnSelectionAllowed(false);
        dataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dataTable.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                updateDataInConfig();
            }
        });
    }

    private void updateDataInConfig() {
        getModelItem().setDataDrivenSourceTable(serializeDataToDataDrivenSourceTableModel(false));
    }

    private Boolean fieldsListContainsField(String name) {
        for (int i = 0; i < fieldsListModel.size(); i++) {
            if (fieldsListModel.elementAt(i).toString().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    private void addFieldButtonActionPerformed(ActionEvent evt) {
        String fieldName = (String) JOptionPane.showInputDialog(
                this,
                "Enter field name:",
                "Add new field",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                "");
        fieldName = fieldName.trim();
        if ((fieldName != null) && (fieldName.length() > 0)) {
            if (fieldsListContainsField(fieldName)) {
                JOptionPane.showMessageDialog(this,
                        "Element " + fieldName + " exists. Please choose another name.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                fieldsListModel.addElement(fieldName);
                dataDrivenSourceTableModel.addColumn(fieldName);
            }
        }
    }

    private void editFieldButtonActionPerformed(ActionEvent evt) {
        int selectedItem = fieldsList.getSelectedIndex();
        String selectedItemValue = fieldsList.getSelectedValue();
        if (selectedItem > -1) {
            String fieldName = (String) JOptionPane.showInputDialog(this,
                    "Enter field name:",
                    "Edit field",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    fieldsListModel.getElementAt(selectedItem)).toString();
            fieldName = fieldName.trim();
            if ((fieldName != null) && (fieldName.length() > 0)) {
                if (fieldsListContainsField(fieldName)) {
                    JOptionPane.showMessageDialog(this,
                            "Element " + fieldName + " exists. Please choose another name.",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    fieldsListModel.setElementAt(fieldName, selectedItem);
                    dataDrivenSourceTableModel.editColumn(selectedItemValue, fieldName);
                }
            }
        }
    }

    private void removeFieldButtonActionPerformed(ActionEvent evt) {
        int selectedFieldIndex = fieldsList.getSelectedIndex();
        String fieldName = fieldsList.getSelectedValue();
        if (selectedFieldIndex > -1) {
            fieldsListModel.remove(selectedFieldIndex);
            dataDrivenSourceTableModel.removeColumn(fieldName);
            // Only for GUI to select element on list after remove item
            if (selectedFieldIndex > fieldsListModel.getSize() - 1) {
                fieldsList.setSelectedIndex(fieldsListModel.getSize() - 1);
            }
            if (fieldsListModel.getSize() > 0) {
                fieldsList.setSelectedIndex(selectedFieldIndex);
            }
        }
    }

    private void addNewLineButtonActionPerformed(ActionEvent evt) {
        if (fieldsListModel.getSize() > 0) {
            Vector values = new Vector();
            values.add(true);
            for (int i = 1; i <= dataDrivenSourceTableModel.getColumnCount(); i++) {
                values.add("");
            }
            dataDrivenSourceTableModel.addRow(values);
        }
    }

    private void removeLineButtonActionPerformed(ActionEvent evt) {
        if (fieldsListModel.getSize() > 0) {
            int selectedRowIndex = dataTable.getSelectedRow();
            if (selectedRowIndex > -1) {
                dataDrivenSourceTableModel.removeRow(selectedRowIndex);
                if (selectedRowIndex == dataDrivenSourceTableModel.getRowCount() && dataDrivenSourceTableModel.getRowCount() > 0) {
                    dataTable.setRowSelectionInterval(selectedRowIndex - 1, selectedRowIndex - 1);
                } else if (selectedRowIndex == 0 && dataDrivenSourceTableModel.getRowCount() > 0) {
                    dataTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
                } else if (selectedRowIndex > 0) {
                    dataTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
                }
            }
        }
    }

    private void moveUpFieldButtonActionPerformed(ActionEvent evt) {
        int selectedItemIndex = fieldsList.getSelectedIndex();
        if (selectedItemIndex > 0) {
            moveField(selectedItemIndex, -1);
        }
    }

    private void moveDownFieldButtonActionPerformed(ActionEvent evt) {
        int selectedItemIndex = fieldsList.getSelectedIndex();
        if (selectedItemIndex < fieldsListModel.getSize() - 1) {
            moveField(selectedItemIndex, 1);
        }
    }

    private void moveField(int index, int position) {
        Object next = fieldsListModel.getElementAt(index + position);
        Object toMove = fieldsListModel.getElementAt(index);
        fieldsListModel.setElementAt(toMove, index + position);
        fieldsListModel.setElementAt(next, index);
        fieldsList.setSelectedIndex(index + position);
        String fieldName = fieldsList.getSelectedValue();
        dataDrivenSourceTableModel.moveColumn(fieldName, position);
    }

    private void moveLineUpButtonActionPerformed(ActionEvent evt) {
        if (fieldsListModel.getSize() > 0) {
            int selectedIndex = dataTable.getSelectedRow();
            if (selectedIndex > 0) {
                dataDrivenSourceTableModel.moveRow(selectedIndex, selectedIndex, selectedIndex - 1);
                dataTable.setRowSelectionInterval(selectedIndex - 1, selectedIndex - 1);
            }
        }
    }

    private void moveLineDownButtonActionPerformed(ActionEvent evt) {
        if (fieldsListModel.getSize() > 0) {
            int selectedIndex = dataTable.getSelectedRow();
            if (selectedIndex > -1 && selectedIndex < dataDrivenSourceTableModel.getRowCount() - 1) {
                dataDrivenSourceTableModel.moveRow(selectedIndex, selectedIndex, selectedIndex + 1);
                dataTable.setRowSelectionInterval(selectedIndex + 1, selectedIndex + 1);
            }
        }
    }

    private void loadFromFileButtonActionPerformed(ActionEvent evt) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("CSV", "csv", "csv"));
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            try {
                File selectedFile = fileChooser.getSelectedFile();
                String fileContent = DataDrivenSourceTestStepHelper.readFileToString(selectedFile);
                loadDataFromCSVString(fileContent, true);
            } catch (Throwable ex) {
                SoapUI.logError(ex);
                JOptionPane.showMessageDialog(this, "Error during load from file:\n" + ex.getMessage());
            }
        }
    }

    private void saveToFileButtonActionPerformed(ActionEvent evt) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("CSV", "csv", "csv"));
        int result = fileChooser.showSaveDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            FileOutputStream fileOutputStream = null;
            try {
                File selectedFile = fileChooser.getSelectedFile();
                String fileContent = DataDrivenSourceTestStepHelper.saveDataDrivenSourceTableModelToCSVString(dataDrivenSourceTableModel, true);
                fileOutputStream = new FileOutputStream(selectedFile);
                if (!selectedFile.exists()) {
                    selectedFile.createNewFile();
                }
                byte[] contentInBytes = fileContent.getBytes();
                fileOutputStream.write(contentInBytes);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Throwable ex) {
                SoapUI.logError(ex);
                JOptionPane.showMessageDialog(this, "Error during save to file:\n" + ex.getMessage());
            } finally {
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Throwable ex) {
                        SoapUI.logError(ex);
                        JOptionPane.showMessageDialog(this, "Error during save to file:\n" + ex.getMessage());
                    }
                }
            }
        }
    }

    private void loadDataFromCSVString(String csvString, Boolean skipEnableColumn) {
        initialize();
        int startIndex = skipEnableColumn ? 1 : 0;
        int lineNumber = 0;
        String[] lines = csvString.split("\r\n|\r|\n");
        for (String line : lines) {
            lineNumber++;
            String[] fields = line.split(Pattern.quote(DataDrivenSourceTestStepHelper.CSVSeparatorForFile), -1);
            if (lineNumber == 1) {
                for (int i = 0; i < fields.length; i++) {
                    dataDrivenSourceTableModel.addColumn(fields[i].trim());
                    fieldsListModel.add(i, fields[i].trim());
                }
            } else {
                Vector row = new Vector();
                if (skipEnableColumn) {
                    row.add(0, true);
                }
                for (int i = startIndex; i <= fields.length; i++) {
                    row.add(i, fields[i - 1]);
                }
                dataDrivenSourceTableModel.addRow(row);
            }
        }
    }

    private DataDrivenSourceTable serializeDataToDataDrivenSourceTableModel(Boolean skipEnableColumn) {
        DataDrivenSourceTable dataDrivenSourceTable = new DataDrivenSourceTable();
        DataDrivenSourceRow header = new DataDrivenSourceRow();
        int startIndex = skipEnableColumn ? 1 : 0;
        for (int i = startIndex; i < dataDrivenSourceTableModel.getColumnCount(); i++) {
            header.add(dataDrivenSourceTableModel.getColumnName(i));
        }
        dataDrivenSourceTable.add(header);
        for (int i = 0; i < dataDrivenSourceTableModel.getRowCount(); i++) {
            DataDrivenSourceRow row = new DataDrivenSourceRow();
            for (int j = startIndex; j < dataDrivenSourceTableModel.getColumnCount(); j++) {
                row.add(dataDrivenSourceTableModel.getValueAt(i, j).toString());
            }
            dataDrivenSourceTable.add(row);
        }
        return dataDrivenSourceTable;
    }

    private void deserializeDataFromDataDrivenSourceTable(DataDrivenSourceTable dataDrivenSourceTable) {
        initialize();
        for (int i = 1; i < dataDrivenSourceTable.get(0).size(); i++) {
            dataDrivenSourceTableModel.addColumn(dataDrivenSourceTable.get(0).get(i));
            fieldsListModel.add(i - 1, dataDrivenSourceTable.get(0).get(i));
        }
        for (int i = 1; i < dataDrivenSourceTable.size(); i++) {
            Vector row = new Vector();
            for (int j = 0; j < dataDrivenSourceTable.get(i).size(); j++) {
                if (j == 0) {
                    row.add(j, Boolean.parseBoolean(dataDrivenSourceTable.get(i).get(j)));
                } else {
                    row.add(j, dataDrivenSourceTable.get(i).get(j));
                }
            }
            dataDrivenSourceTableModel.addRow(row);
        }
        updateDataInConfig();
    }

    private void selectAllButtonActionPerformed(ActionEvent evt) {
        for (int i = 0; i < dataTable.getRowCount(); i++) {
            dataTable.setValueAt(true, i, 0);
        }
    }

    private void deselectAllButtonActionPerformed(ActionEvent evt) {
        for (int i = 0; i < dataTable.getRowCount(); i++) {
            dataTable.setValueAt(false, i, 0);
        }
    }

    private void initComponents() {

        fieldsListScrollPane1 = new JScrollPane();
        fieldsList = new JList<>();
        addFieldButton = new JButton();
        removeFieldButton = new JButton();
        editFieldButton = new JButton();
        moveUpFieldButton = new JButton();
        moveDownFieldButton = new JButton();
        dataTableScrollPane = new JScrollPane();
        dataTable = new JTable();
        AddNewLineButton = new JButton();
        RemoveLineButton = new JButton();
        moveLineUpButton = new JButton();
        moveLineDownButton = new JButton();
        loadFromFileButton = new JButton();
        saveToFileButton = new JButton();
        selectAllButton = new JButton();
        deselectAllButton = new JButton();
        headerBackgroudPanel = new JPanel();
        headerLogoLabel = new JLabel();
        headerLabel = new JLabel();
        headerBackgroudPanel1 = new JPanel();

        fieldsListScrollPane1.setViewportView(fieldsList);

        addFieldButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/add.png"));
        addFieldButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                addFieldButtonActionPerformed(evt);
            }
        });

        removeFieldButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/remove.png"));
        removeFieldButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                removeFieldButtonActionPerformed(evt);
            }
        });

        editFieldButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/rename.png"));
        editFieldButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                editFieldButtonActionPerformed(evt);
            }
        });

        moveUpFieldButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/up.png"));
        moveUpFieldButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                moveUpFieldButtonActionPerformed(evt);
            }
        });

        moveDownFieldButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/down.png"));
        moveDownFieldButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                moveDownFieldButtonActionPerformed(evt);
            }
        });

        dataTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{

                },
                new String[]{

                }
        ));
        dataTableScrollPane.setViewportView(dataTable);

        AddNewLineButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/add.png"));
        AddNewLineButton.setName("");
        AddNewLineButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                addNewLineButtonActionPerformed(evt);
            }
        });

        RemoveLineButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/remove.png"));
        RemoveLineButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                removeLineButtonActionPerformed(evt);
            }
        });

        moveLineUpButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/up.png"));
        moveLineUpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                moveLineUpButtonActionPerformed(evt);
            }
        });

        moveLineDownButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/down.png"));
        moveLineDownButton.setToolTipText("");
        moveLineDownButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                moveLineDownButtonActionPerformed(evt);
            }
        });

        loadFromFileButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/open.png"));
        loadFromFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                loadFromFileButtonActionPerformed(evt);
            }
        });

        saveToFileButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/save.png"));
        saveToFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                saveToFileButtonActionPerformed(evt);
            }
        });

        selectAllButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/selectall.png"));
        selectAllButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                selectAllButtonActionPerformed(evt);
            }
        });

        deselectAllButton.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/deselectall.png"));
        deselectAllButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                deselectAllButtonActionPerformed(evt);
            }
        });

        headerBackgroudPanel.setBackground(new Color(249, 249, 249));

        headerLogoLabel.setIcon(UISupport.createImageIcon("DataDrivenSourceTestStep/icons/header.png"));

        headerLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
        headerLabel.setText("Data driven source");

        headerBackgroudPanel1.setBackground(java.awt.SystemColor.controlShadow);
        headerBackgroudPanel1.setPreferredSize(new java.awt.Dimension(1058, 1));

        javax.swing.GroupLayout headerBackgroudPanel1Layout = new javax.swing.GroupLayout(headerBackgroudPanel1);
        headerBackgroudPanel1.setLayout(headerBackgroudPanel1Layout);
        headerBackgroudPanel1Layout.setHorizontalGroup(
                headerBackgroudPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 0, Short.MAX_VALUE)
        );
        headerBackgroudPanel1Layout.setVerticalGroup(
                headerBackgroudPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout headerBackgroudPanelLayout = new javax.swing.GroupLayout(headerBackgroudPanel);
        headerBackgroudPanel.setLayout(headerBackgroudPanelLayout);
        headerBackgroudPanelLayout.setHorizontalGroup(
                headerBackgroudPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(headerBackgroudPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(headerLogoLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(headerLabel)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(headerBackgroudPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
        );
        headerBackgroudPanelLayout.setVerticalGroup(
                headerBackgroudPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(headerBackgroudPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(headerBackgroudPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(headerLogoLabel)
                                        .addGroup(headerBackgroudPanelLayout.createSequentialGroup()
                                                .addGap(9, 9, 9)
                                                .addComponent(headerLabel)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(headerBackgroudPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(fieldsListScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(addFieldButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(removeFieldButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(editFieldButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(moveUpFieldButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(moveDownFieldButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(8, 8, 8)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(loadFromFileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(saveToFileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(selectAllButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(deselectAllButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addComponent(dataTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(moveLineUpButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addComponent(RemoveLineButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addComponent(AddNewLineButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(moveLineDownButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6))
                        .addComponent(headerBackgroudPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(headerBackgroudPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(removeFieldButton)
                                                        .addComponent(editFieldButton)
                                                        .addComponent(moveUpFieldButton)
                                                        .addComponent(moveDownFieldButton)
                                                        .addComponent(addFieldButton))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(fieldsListScrollPane1))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(loadFromFileButton)
                                                        .addComponent(saveToFileButton)
                                                        .addComponent(selectAllButton)
                                                        .addComponent(deselectAllButton, javax.swing.GroupLayout.Alignment.TRAILING))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(dataTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(AddNewLineButton)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(RemoveLineButton)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(moveLineUpButton)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(moveLineDownButton)
                                                                .addGap(0, 0, Short.MAX_VALUE)))))
                                .addContainerGap())
        );
    }

}
